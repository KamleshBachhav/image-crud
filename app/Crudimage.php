<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crudimage extends Model
{
    protected $fillable = ['fstName','lstName','image'];
}
