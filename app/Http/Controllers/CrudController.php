<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Pagination\Paginator;
use App\Crudimage;
use Validator;
// use App\User;
use Storage;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $crud = Crudimage::all();
        // return view('index', compact('crud'));
        return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //Validation rules Starts here -->
        // $rules =array(
        //     'fstName'=>'required',
        //     'lstName'=>'required',
        //     'img'=>'required|max:2048|mimes:jpg',
        // );

        // $messages = array(
        //     'img.mimes' => 'select file type " .jpg only "',
        //     'img.max' => 'select a file upto 2mb ',
        // );

        // $validator = Validator::make($request->all(), $rules,$messages);
        //     //Check to see if validation fails or passes
        // if($validator->fails())
        // {
        //     // Alert::error('Error','Somthing went wrong.. Please check errors..! ');
        //     return redirect()->back()->withErrors($validator)->withInput();
        // }

        $crud = new Crudimage();
        $crud->fstName = $request->fst;
        $crud->lstName = $request->lst;
        if ($request->file('img')) {
            $upldFile = $request->img;
            $extension = $upldFile->getClientOriginalExtension();
            $filename = $upldFile->getFilename().'.'.$extension;
            Storage::disk('local')->put('public/uploads/'.$filename, File::get($upldFile));
            
            $crud->image = $filename;
        }
            $crud->save();
        return redirect('/cruds')->with('success','User with Image has been added..!');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $crud = Crudimage::get(); //->paginate(5);
        return view('show', compact('crud'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Crudimage::find($id);
        return view('edit',compact('data','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Crudimage::findOrFail($id);
        $data->fstName = $request->fst;
        $data->lstName = $request->lst;
        
        if ($request->file('img')) {
            
            $upldFile = $request->img;
            $extension = $upldFile->getClientOriginalExtension();
            $filename = $upldFile->getFilename().'.'.$extension;
            Storage::disk('local')->put('public/uploads/'.$filename, File::get($upldFile));
            
            //delete old image
            // $oldFilename=$data->image;
            $mult[] = $filename;
            $data->image = json_encode($mult);
            // $aboutus->image=$filename;
            // Storage::delete($oldFilename);
        }
            $data->save();
        return redirect('/cruds/show')->with('success','User with Image has been updated..!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return ($id);
        $crud = Crudimage::findOrFail($id);
        // $path = storage_path('public/uploads/'. $crud->image);
        // if(file_exists($path))
        // {
        //     unlink($path);
        // }
        $crud->delete();
        return redirect()->back()->with('success', 'User is successfully deleted');

    }
}
