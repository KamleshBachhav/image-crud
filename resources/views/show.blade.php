<!DOCTYPE html>
<html>
<head>
	<title>Upload Image</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="card ">
	  <div class="card-header">
	  
	  </div>
	  <div class="card-body">
	    @if ($errors->any())
	      <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	      </div><br />
	    @endif
	<div class="Container ">
		<div class="col-md-12 justify-content-center">
		<div class="col-md-10" style="margin-left: 80px;" >
			<h2 style="text-align: center;">Record Details</h2>
			<a href="/cruds" class="btn btn-primary" style="float: left;">Create Page</a>
			<br/><br/>
			<table class="table table-bordered table-striped" style="text-align: center;" >
				<tr>
					<th style="text-align: center;">No.</th>
					<th style="text-align: center;">First Name</th>
					<th style="text-align: center;">Last Name</th>
					<th style="text-align: center;">Profile</th>
					<th style="text-align: center;">Action</th>
				</tr>
				<?php $i = 1;  ?>
				@foreach($crud as $prof)
				<tr>
					<td>{{ $i }}</td>
					<td>{{$prof->fstName}}</td>
					<td>{{$prof->lstName}}</td>
					<td><img src="{{Storage::url('uploads/'. $prof->image)}}" style="height: 70px; width: 70px; border-radius: 50px;" alt="picture"></td>
					<td><form action="{{ route('cruds.destroy', $prof->id)}}" method="post">
						<a href="{{route('cruds.edit', $prof->id)}}" class="btn btn-primary">Edit</a>
		                  @csrf
		                  <!-- @method('DELETE') -->
		                  <button class="btn btn-primary" type="submit" >Delete</button>
		                </form>
					</td>
				</tr>
				<?php $i++; ?>
				@endforeach
			</table>
		</div>
		</div>
	</div>
</body>
</html>