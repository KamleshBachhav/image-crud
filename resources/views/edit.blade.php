<!DOCTYPE html>
<html>
<head>
	<title>Upload Image</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container" >
		<div class=" row col-md-8">
		<!-- <form action="{{route('cruds.update', $data->id)}}" method="PATCH" enctype="multipart/form-data"> -->
		<form action="/upload/{{ $data->id }}" method="POST" enctype="multipart/form-data">
			@csrf
			<!-- {{ method_field('PATCH') }} -->
			<h2>Image Upload</h2>
			<div class="form-group">
				<label>First Name :</label>
				<input type="text" name="fst" class="form-control" value="{{$data->fstName}}" required autofocus>
			</div><br/>
			<div class="form-group">
				<label>Last Name :</label>
				<input type="text" name="lst" class="form-control" value="{{$data->lstName}}" required autofocus>
			</div><br/>
			<div class="form-group">
				<img src="{{Storage::url('uploads/'. $data->image)}}" width="70" height="70"><br/>
				<label>Upload Image :</label>
				<input type="file" name="img" required autofocus>
			</div><br/>
			<button type="submit" class="btn btn-primary">Update</button>
			<!-- <div class="form-group">
				<input type="submit" class="btn btn-primary" >  -->
			<!-- </div> -->
		</form>
		</div>
	</div>
</body>
</html>