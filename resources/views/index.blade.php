<!DOCTYPE html>
<html>
<head>
	<title>CRUD IMAGE OPERATION</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container"  >
			<div class="col-md-8 offset-md-2">
			@if ($errors->any())
		          <div class="alert alert-danger">
		              <ul>
		                  @foreach ($errors->all() as $error)
		                      <li>{{ $error }}</li>
		                  @endforeach
		              </ul>
		          </div>
		      @endif
		    </div>	
		<div class=" row col-md-6">
		<form action="{{route('cruds.store')}}" method="post" enctype="multipart/form-data">
			@csrf
			<h2 style="text-align: center;">Image Upload</h2>
			<div class="form-group">
				<label>First Name :</label>
				<input type="text" name="fst" class="form-control" required autofocus>
			</div><br/>
			<div class="form-group">
				<label>Last Name :</label>
				<input type="text" name="lst" class="form-control" required autofocus>
			</div><br/>
			<div class="form-group">
				<label>Upload Image :</label>
				<input type="file" name="img" autofocus >
			</div><br/>
			<div class="form-group">
				<input type="submit"  class="btn btn-primary" > 
				<a href="/cruds/show" target="_blank" class="btn btn-primary" style="float: right;" > Details </a>  
			</div>
		</form>
		</div>
	</div>
</body>
</html>