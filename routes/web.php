<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('cruds','CrudController');
Route::post('/upload/{id}','CrudController@update');

// Route::get('/index','CrudController@index');
// Route::post('/store','CrudController@store');
// Route::get('/show','CrudController@show');
// Route::post('/delete/{id}','CrudController@destroy');
